# Doghouse Google Tag Manager

Embeds the Google Tag Manager scripts in a website.

## Installation

Install using Composer (preferred) or Modman.

## Usage

Preferred way it to use it as an instance widget. Install module, go to CMS -> Widgets -> Add New Widget Instance. Display on all pages with block reference `Page Top` (which is `after_body_start`).

## About

Initially created for My Health Warehouse