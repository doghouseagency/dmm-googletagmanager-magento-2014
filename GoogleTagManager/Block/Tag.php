<?php

class Doghouse_GoogleTagManager_Block_Tag
    extends Mage_Core_Block_Template
    implements Mage_Widget_Block_Interface
{

    /**
     * Checks if module is enabled.
     *
     * @return mixed
     */
    public function isEnabled()
    {
        return Mage::getStoreConfig(sprintf(
            '%s/active',
            Doghouse_GoogleTagManager_Model_Config::XML_NAMESPACE
        ));
    }

    /**
     * Returns the unique Id of Google Tag Manager
     *
     * @return mixed
     */
    protected function _getTagId()
    {
        return Mage::getStoreConfig(sprintf(
            '%s/tag_id',
            Doghouse_GoogleTagManager_Model_Config::XML_NAMESPACE
        ));
    }

    /**
     * Returns the data layer id if set in Widget
     *
     * @return string
     */
	public function getDatalayerId()
    {
		$datalayer = parent::getDatalayerId();
		if(!$datalayer) {
			$datalayer = "dataLayer";
		}
		return $datalayer;
	}

	public function getCloudflareParams()
    {
		if($this->getCloudflareInUse()) {
			return ' data-cfasync="false"';
		}
		return '';
	}

    /**
    * If Global is active, set the tag and template.
    * @return Varien_Object
    */
    public function setDefault()
    {
        if (!$this->isEnabled()) {
            return $this;
        }

        $tagId = $this->_getTagId();
        if (!$tagId) {
            return $this;
        }

        return $this->setData('tag_id', $tagId);
    }

}
